Implements xc3sprog 'indirect programming' for the Spartan 6 XC6SLX150T, such as used on the Enterpoint Milldown boards. HDLs and UCFs adapted from xc3sprog.

To run this, 

 * just create a new ISE project using the Spartan 6 family, XC6SLX150T, FGG900, speed -2, in this folder.
 * Add all the source files. Ignore the error about bscan_common.
 * Compile and generate a bitstream.
 * Program the bitstream using xc3sprog.

Note that on the Milldowns, you must disable backplane access to the
JTAG via the four switches SW2, otherwise the programming doesn't seem
to work.